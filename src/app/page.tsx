import {
  getStoryblokApi,
} from "@storyblok/react/rsc";
import StoryblokStory from "@storyblok/react/story";
import {ISbStoriesParams} from "@storyblok/react";

export default async function Home() {
  const { data } = await fetchData();

  return (
      <div>
        <StoryblokStory story={data.story} />
      </div>
  );
}

async function fetchData() {
  let sbParams: ISbStoriesParams = { version: "draft" };

  const storyblokApi = getStoryblokApi();
  return await storyblokApi.get(`cdn/stories/home`, sbParams, {cache: "no-store"});
}