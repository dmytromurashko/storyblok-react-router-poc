import type { Metadata } from "next";
import "./globals.css";
import {apiPlugin, storyblokInit} from "@storyblok/react";
import Feature from "@/components/Feature";

export const metadata: Metadata = {
  title: "Zauner Group",
};

storyblokInit({
  accessToken: '2R5J8b6jWMzK6D93pjN0qQtt',
  use: [apiPlugin],
  components: {
    feature: Feature,
  }
})

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
