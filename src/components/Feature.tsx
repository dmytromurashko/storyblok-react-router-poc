import { storyblokEditable } from "@storyblok/react/rsc";
import { FC } from "react";

type Props = {
  blok: {
    name?: string;
  };
};

const Feature: FC<Props> = ({ blok }) => (
  <div {...storyblokEditable(blok)}>{blok.name}</div>
);

export default Feature;
