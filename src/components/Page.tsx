import { StoryblokComponent, storyblokEditable } from "@storyblok/react/rsc";
import type { SbBlokData } from "@storyblok/react";
import { FC } from "react";

type Props = {
    blok: {
        header: SbBlokData[];
        body: SbBlokData[];
    };
};

const Page: FC<Props> = ({ blok }) => {
    return (
        <div>
            {blok.header?.map((nestedBlok) => (
                <StoryblokComponent
                    blok={nestedBlok}
                    key={nestedBlok._uid}
                />
            ))}
            <main {...storyblokEditable(blok)}>
                {blok.body?.map((nestedBlok) => (
                    <StoryblokComponent
                        blok={nestedBlok}
                        key={nestedBlok._uid}
                    />
                ))}
            </main>
        </div>
    );
};

export default Page;
